#include <mutex>
#include <thread>
#include <unistd.h>

void f (std::mutex & mtx) {
    for (int i = 0; i < 6; ++i) {
        mtx.lock();
        sleep(2);
        mtx.unlock();
    }
}

int main () {
    std::mutex mtx;
    std::thread th([&](){f(mtx);});

    for (int i = 0; i < 6; ++i) {
        mtx.lock();
        mtx.unlock();
    }

    th.join();
}
