find_package(Threads)

add_executable(mutracktest
    test/test.cpp
)
target_link_libraries(mutracktest
    LINK_PRIVATE
        ${CMAKE_THREAD_LIBS_INIT}
)
