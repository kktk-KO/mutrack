find_package(Threads)

add_library(mutrack MODULE
    src/mutrack/mutrack.cpp
    src/mutrack/backtrace.cpp
    src/mutrack/context.cpp
)
target_link_libraries(mutrack
    LINK_PRIVATE
        ${CMAKE_DL_LIBS}
        ${CMAKE_THREAD_LIBS_INIT}
)
set_property(TARGET mutrack PROPERTY POSITION_INDEPENDENT_CODE ON)

install(TARGETS mutrack LIBRARY DESTINATION ${LIB_INSTALL_DIR})
install(PROGRAMS bin/mutrack.bash DESTINATION bin RENAME mutrack)
