function(PREPEND var prefix)
   set(listVar "")
   foreach(f ${ARGN})
      list(APPEND listVar "${prefix}/${f}")
   endforeach(f)
   set(${var} "${listVar}" PARENT_SCOPE)
endfunction(PREPEND)

file(GLOB_RECURSE MUTRACK_CLI_FILES RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}/src/mutrack-cli" "*.py")
prepend(MUTRACK_CLI_FILES src/mutrack-cli ${MUTRACK_CLI_FILES})

install(FILES ${MUTRACK_CLI_FILES} DESTINATION lib/mutrack-cli)
install(PROGRAMS bin/mutrack-cli.bash DESTINATION bin RENAME mutrack-cli)
