#pragma once

#include <array>

namespace mutrack {

struct Backtrace {

    using Id = std::size_t;

    Backtrace ();

    Id  GetId () const noexcept;
    void SetId (Id id) noexcept;

    void Serialize () const;

    bool operator== (Backtrace const& other) const noexcept;
    bool operator!= (Backtrace const& other) const noexcept;

    static Backtrace GetCurrentStack ();

    struct Hash {
        std::size_t operator() (Backtrace const& k) const noexcept;
    };

private:
    Id id_;
    std::array<void *, 128> returnAddrList_;
    std::size_t size_;

};

}
