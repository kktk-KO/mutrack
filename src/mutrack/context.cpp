#include "context.hpp"

#include <cstdio>
#include <cstdlib>
#include <chrono>
#include <dlfcn.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>

namespace mutrack {

Context::Context () {
    this->lock_ = (LockFn) dlsym(RTLD_NEXT, "pthread_mutex_lock");
    this->unlock_ = (UnlockFn) dlsym(RTLD_NEXT, "pthread_mutex_unlock");
}

int Context::Lock (pthread_mutex_t * mutex) {
    thread_local bool isActive = false;
    if (isActive) {
        return lock_(mutex);
    }
    isActive = true;
    auto bt = Backtrace::GetCurrentStack();
    auto btId = Register(bt);
    auto t1 = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::steady_clock::now().time_since_epoch()).count();
    auto ret = lock_(mutex);
    auto t2 = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::steady_clock::now().time_since_epoch()).count();
    std::printf("lock,%u,%p,%zu,%zu,%zu\n", btId, mutex, syscall(SYS_gettid), t1, t2);
    isActive = false;
    return ret;
}

int Context::Unlock (pthread_mutex_t * mutex) {
    return unlock_(mutex);
}

Backtrace::Id Context::Register (Backtrace & bt) {
    std::lock_guard<std::mutex> lock(btLock_);
    auto it = btMap_.find(bt);
    if (it == btMap_.end()) {
        auto btId = this->btId_++;
        bt.SetId(btId);
        btMap_.emplace_hint(it, bt, btId);
        bt.Serialize();
        return btId;
    } else {
        return it->second;
    }
}

}
