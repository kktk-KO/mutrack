#include "context.hpp"

namespace mutrack {

static Context ctx;

}

extern "C" {

int pthread_mutex_lock(pthread_mutex_t* mutex) {
    return mutrack::ctx.Lock(mutex);
}

int pthread_mutex_unlock(pthread_mutex_t* mutex) {
    return mutrack::ctx.Unlock(mutex);
}

}
