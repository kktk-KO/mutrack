#pragma once

#include <pthread.h>
#include <bits/pthreadtypes.h>
#include <mutex>
#include <unordered_map>

#include "backtrace.hpp"

namespace mutrack {

struct Context {
    using LockFn = int (*) (pthread_mutex_t*);
    using UnlockFn = int (*) (pthread_mutex_t*);
    using BacktraceId = unsigned int;
    using BacktraceIdMap = std::unordered_map<Backtrace, BacktraceId, Backtrace::Hash>;

    Context ();

    int Lock (pthread_mutex_t * mutex);

    int Unlock (pthread_mutex_t * mutex);

    Backtrace::Id Register (Backtrace & bt);
private:

    LockFn lock_;
    UnlockFn unlock_;

    BacktraceIdMap btMap_;
    BacktraceId btId_ = 0;
    std::mutex btLock_;


};

}


