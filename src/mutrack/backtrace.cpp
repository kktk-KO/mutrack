#include "backtrace.hpp"

#include <execinfo.h>

namespace mutrack {

Backtrace::Backtrace ()
: id_{0}
, returnAddrList_{nullptr}
, size_{0}
{}

std::size_t Backtrace::GetId () const noexcept {
    return this->id_;
}

void Backtrace::SetId (std::size_t id) noexcept {
    this->id_ = id;
}

void Backtrace::Serialize () const {
    std::printf("bt,%u", this->id_);
    if (this->size_ == 0) {
        return;
    }
    char ** names = backtrace_symbols(this->returnAddrList_.data(), this->size_);
    if (names == NULL) {
        std::printf("\n");
        return;
    }
    for (size_t i = 0; i < this->size_; ++i) {
        std::printf(",%s", names[i]);
    }
    std::printf("\n");
    free(names);
}

bool Backtrace::operator== (Backtrace const& other) const noexcept {
    if (this->size_ != other.size_) {
        return false;
    }
    for (std::size_t i = 0; i < this->size_; ++i) {
        if (this->returnAddrList_[i] != other.returnAddrList_[i]) {
            return false;
        }
    }
    return true;
}

bool Backtrace::operator!= (Backtrace const& other) const noexcept {
    return !(*this == other);
}

Backtrace Backtrace::GetCurrentStack () {
    auto bt = Backtrace();
    bt.size_ = backtrace(bt.returnAddrList_.data(), bt.returnAddrList_.size() - 1);
    return bt;
}

std::size_t Backtrace::Hash::operator() (Backtrace const& k) const noexcept {
    std::size_t v = 0;
    for (auto c: k.returnAddrList_) {
        if (c == nullptr) {
            break;
        }
        v ^= std::hash<void *>()(c) + 0x9e3779b9 + (v << 6) + (v >> 2);
    }
    return v;
}

}
