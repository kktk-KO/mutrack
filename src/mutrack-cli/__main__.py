#!/usr/bin/env python

from future.utils import viewvalues

import argparse
import numpy

def Main():
    parser = argparse.ArgumentParser(
        description='mutrack-cli'
    )
    parser.add_argument('file', help='File to load.')
    options = parser.parse_args()

    bts = {}
    locks = []
    with open(options.file) as f:
        while True:
            line = f.readline()
            if len(line) == 0:
                break
            data = line.strip().split(',')
            if data[0] == 'bt':
                btId = int(data[1])
                bts[btId] = {
                    'id': btId,
                    'returnAddressList': data[2:],
                    'lockCount': 0,
                    'lockDurationList': [],
                    'lockDurationTotal': 0.0,
                    'lockDurationAverage': 0.0,
                    'lockDurationMedian': 0.0,
                    'lockDurationMin': 0.0,
                    'lockDurationMax': 0.0,
                }
            if data[0] == 'lock':
                locks.append({
                    'backtraceId': int(data[1]),
                    'mutexAddr': data[2],
                    'threadId': int(data[3]),
                    'lockStart': int(data[4]) * 1e-9,
                    'lockEnd': int(data[5]) * 1e-9,
                    'lockDuration': (int(data[5]) - int(data[4])) * 1e-9
                })
    for lock in locks:
        bts[lock['backtraceId']]['lockCount'] += 1
        bts[lock['backtraceId']]['lockDurationList'].append(lock['lockDuration'])
        bts[lock['backtraceId']]['lockDurationTotal'] += lock['lockDuration']

    for bt in viewvalues(bts):
        bt['lockDurationAverage'] = 1.0 * bt['lockDurationTotal'] / bt['lockCount']
        bt['lockDurationMedian'] = numpy.median(bt['lockDurationList'])
        bt['lockDurationMin'] = numpy.min(bt['lockDurationList'])
        bt['lockDurationMax'] = numpy.max(bt['lockDurationList'])
        del bt['lockDurationList']

    import IPython;
    IPython.embed()

if __name__ == '__main__':
    Main()
